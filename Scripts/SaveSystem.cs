using UnityEngine;
using System.Collections;

public class SaveSystem : MonoBehaviour {


	//public PlayerPrefs playerPrefs;
	public int level;
	public int Presents=0;
	public DamageSystem playerDamage;
	public GameObject wallAI;
	private AIStrategy aiStrategy;
	
	void Awake()
	{

		playerDamage = GameObject.FindGameObjectWithTag("Player").GetComponent<DamageSystem>();
		aiStrategy = GameObject.FindGameObjectWithTag("AIStrategy").GetComponent<AIStrategy>();
	}
	
	void Start()
	{
		//SetGame(9,9);
		//NewGame();
		//Load();	
		//PlayerPrefs.DeleteAll();
	}
	
	public void Save()
	{
		PlayerPrefs.SetInt("Level",level);
		PlayerPrefs.SetInt("Presents",Presents);
		print("Saved level as "+level + "with " +Presents+"Presents!");
	}
	
	public void Load()
	{
		level = PlayerPrefs.GetInt("Level",0);
		Presents = PlayerPrefs.GetInt("Presents",0);
//		print("Loaded level"+level);
		if(level>=10)
		{
			Application.LoadLevel("Winner");
		}
		aiStrategy.NewWave();
		Presents = InitPresents(level);
		/*if(level>=10)
		{
			aiStrategy.SetEnemies(1,1,1);
			//Application.LoadLevel("Winner");
		}*/
		
		
		
		aiStrategy.SummonedAllies = 0 ;
		
		
		
		aiStrategy.ReCalculateNPCs();
		aiStrategy.AssignTargets("Enemy");
		
	}
	
	public void NewGame()
	{
		PlayerPrefs.SetInt("Level",0);
		PlayerPrefs.SetInt("Presents",100);
		print("NewGame:Saved level as "+level + "with " +Presents+"Presents!");
	}
	
	public void SetGame(int level,int Presents)
	{
		PlayerPrefs.SetInt("Level",level);
		PlayerPrefs.SetInt("Presents",Presents);
		print("Saved level as "+level + "with " +Presents+"Presents!");
	}
	
	public int InitPresents(int level)
	{
		int Presents;
		switch(level)
		{
			case 0:
				Presents = 3;
				break;
			case 1:
				Presents = 6;
				break;
			case 2:
				Presents = 7;
				break;
			case 3:
				Presents = 9;
				break;
			case 4:
				Presents = 9;
				break;
			case 5:
				Presents = 15;
				break;
			case 6:
				Presents = 10;
				break;
			case 7:
				Presents = 14;
				break;
			case 8:
				Presents = 20;
				break;
			case 9:
				Presents = 9;
				break;
			default:
				Presents = 10;
				break;
		}
		return Presents;
	}
	
}
