using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour {
	
	public Animation runAnimator;
	public Animation fireAnimator;
	public Animation idleAnimator;
	
	private float fireAnimationTimer=0.0f;
	private bool fireAnimationTimerEnable=true;
	
	public Renderer runMesh;
	public Renderer fireMesh;
	public Renderer idleMesh;
	
	
	public void Fire()
	{	
		fireMesh.enabled=true;
		runMesh.enabled =false;
		idleMesh.enabled=false;
		
		fireAnimator.enabled=true;
		idleAnimator.enabled=false;
		runAnimator.enabled=false;
		
		fireAnimator.Stop();
		fireAnimator.Rewind();
		fireAnimator.Play();
	}
	
	public void Run()
	{
		runMesh.enabled =true;
		idleMesh.enabled=false;
		fireMesh.enabled=false;
		
		runAnimator.enabled=true;
		idleAnimator.enabled=false;
		fireAnimator.enabled=false;
	}
	
	public void Idle()
	{
		idleMesh.enabled=true;
		runMesh.enabled =false;
		fireMesh.enabled=false;

		idleAnimator.enabled=true;
		runAnimator.enabled=false;
		fireAnimator.enabled=false;
	}


}
