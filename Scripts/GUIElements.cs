using UnityEngine;
using System.Collections;

public class GUIElements : MonoBehaviour {
	public GUIStyle style;
	public Font font;
	
	public Transform GoblinsIcon;
	public Transform ElfsIcon;
	public Transform SantaIcon;
	public Transform WallIcon;
	
	public CameraMovement cameraMovement;
	
	
	public Camera MainCamera;
	public Camera ElfCamera;
	private Camera GameplayCamera;
	
	public Vector3 HitGoblin;
	
	public Transform FocusPlaneIcon;
	public Transform FocusPlaneBIcon;
	
	//Points
	public float PointMessageOffsetX=0f;
	public float PointMessageOffsetY=0f;
	
	
	//FocusPlane
	public float FocusPlaneOffsetX=0f;
	public float FocusPlaneOffsetY=0f;
	public float FocusPlaneBOffsetX=0f;
	public float FocusPlaneBOffsetY=0f;
	
	private float BendFactor1=1f;
	private float BendFactor2=1f;
	
	//GOBLIN
	public float GoblinsIconOffsetX=10f;
	public float GoblinsIconOffsetY=10f;
	public float GoblinMessageOffsetX=10f;
	public float GoblinMessageOffsetY=10f;
	
	//ELF
	public float ElfsIconOffsetX=10.0f;
	public float ElfsIconOffsetY=10.0f;
	public float ElfMessageOffsetX = 10.0f;
	public float ElfMessageOffsetY = 10.0f;
	
	//SANTA
	public float SantaIconOffsetX = 10.0f;
	public float SantaIconOffsetY = 10.0f;
	public float SantaMessageOffsetX = 10.0f;
	public float SantaMessageOffsetY = 10.0f;
	
	//Wall
	public float WallIconOffsetX=10.0f;
	public float WallIconOffsetY = 10.0f;
	public float WallMessageOffsetX = 10.0f;
	public float WallMessageOffsetY = 10.0f;
	
	
	//Level info
	public float LevelMessageOffsetX=10.0f;
	public float LevelMessageOffsetY=10.0f;
	
	//Presents
	public float PresentsMessageOffsetX = 10.0f;
	public float PresentsMessageOffsetY = 10.0f;
	
	//Pause
	public float PauseMessageOffsetX = 10.0f;
	public float PauseMessageOffsetY = 10.0f;
	
	private AIStrategy aiStrategy;
	public WallAI wallAI;
	private float wallInitialLife=0.0f;
	
	private DamageSystem PlayerDamageSystem;
	private float PlayerInitialLife=0.0f;
	
	public SaveSystem saveSystem;
	
	private float InitTimeScale;
	
	public float PauseTime;
	private float PauseTimeInstance;
	private float PauseTimeTimer=0.0f;
	private bool hasTimeSample=false;
	
	public bool gamePlay = false;
	
	private bool PresentShowerLock=false;
	public Object PresentPrefab;
	public  Transform PresentInst;
	
	private GameObject ObjectPointer;
	private int InitPresents;
	
	public PlayerInput playerInput;
	
	void Awake()
	{
		aiStrategy = GameObject.FindGameObjectWithTag("AIStrategy").GetComponent<AIStrategy>();
		wallInitialLife = wallAI.Life;
		
		PlayerDamageSystem = GameObject.FindGameObjectWithTag("Player").GetComponent<DamageSystem>();
	}
	
	void Start()
	{
		saveSystem.Load();
		InitPresents = saveSystem.InitPresents(saveSystem.level);
		ObjectPointer = new GameObject();
		PlayerInitialLife = PlayerDamageSystem.Life;
		InitTimeScale = Time.timeScale;
	}
	
	void Update()
	{
		if(aiStrategy.playerTarget&&cameraMovement.currentCameraAngle==CameraMovement.CameraAngle.Bird)
		{
			//FocusPlaneBIcon.renderer.enabled=true;
			//FocusPlaneIcon.renderer.enabled=true;
			BendFactor1 = Mathf.Lerp(BendFactor1,1f,0.1f);
			BendFactor2 = Mathf.Lerp(BendFactor2,1f,0.1f);
		}
		else
		{
			BendFactor1 = Mathf.Lerp(BendFactor1,-0.5f,0.1f);
			BendFactor2 = Mathf.Lerp(BendFactor2,1.1f,0.1f);
		}
		
		if(ElfCamera.enabled)
		{
			GameplayCamera=ElfCamera;
		}
		else
		{
			GameplayCamera = MainCamera;
		}
			
		//	FocusPlaneIcon.localScale.y= Mathf.Lerp(FocusPlaneOffsetY.localScale.y,0f,1f);
		//print(FocusPlaneIcon.localScale.y);
		
	}
	
    void OnGUI() 
	{
			FocusPlaneIcon.position = camera.ScreenToWorldPoint(new Vector3(Screen.width/FocusPlaneOffsetX,(Screen.height/FocusPlaneOffsetY)*BendFactor1 ,8.1f));
			FocusPlaneBIcon.position = camera.ScreenToWorldPoint(new Vector3(Screen.width/FocusPlaneBOffsetX,(Screen.height/FocusPlaneBOffsetY)*BendFactor2 ,8.1f));
		
			GoblinsIcon.position = camera.ScreenToWorldPoint(new Vector3(Screen.width/GoblinsIconOffsetX,Screen.height/GoblinsIconOffsetY,8.004776f));
			GUI.Box(new Rect(Screen.width/GoblinMessageOffsetX,Screen.height/GoblinMessageOffsetY,Screen.width/5,Screen.height/5),"x "+aiStrategy.EnemyNPCS.Length,style);
			
			ElfsIcon.position = camera.ScreenToWorldPoint(new Vector3(Screen.width/ElfsIconOffsetX,Screen.height/ElfsIconOffsetY,8.004776f));
			GUI.Box(new Rect(Screen.width/ElfMessageOffsetX,Screen.height/ElfMessageOffsetY,Screen.width/5,Screen.height/5),aiStrategy.SummonedAllies+" | "+(aiStrategy.SummonedAlliesLimit-aiStrategy.SummonedAllies),style);
			
			SantaIcon.position = camera.ScreenToWorldPoint(new Vector3(Screen.width/SantaIconOffsetX,Screen.height/SantaIconOffsetY,8.004776f));
			GUI.Box(new Rect(Screen.width/SantaMessageOffsetX,Screen.height/SantaMessageOffsetY,Screen.width/5,Screen.height/5),((PlayerDamageSystem.Life/PlayerInitialLife)*100.0f).ToString("#.##") + "%",style);
			
			WallIcon.position = camera.ScreenToWorldPoint(new Vector3(Screen.width/WallIconOffsetX,Screen.height/WallIconOffsetY,8.004776f));
			GUI.Box(new Rect(Screen.width/WallMessageOffsetX,Screen.height/WallMessageOffsetY,Screen.width/5,Screen.height/5),((wallAI.Life/wallInitialLife)*100.0f).ToString("#.##") + "%",style);
		
			GUI.Box(new Rect(Screen.width/PresentsMessageOffsetX,Screen.height/PresentsMessageOffsetY,Screen.width/5,Screen.height/5),saveSystem.Presents+"/"+InitPresents,style);
			if(HitGoblin!=null)
				GUI.Box(new Rect(camera.WorldToScreenPoint(HitGoblin).x,camera.WorldToScreenPoint(HitGoblin).y,Screen.width/5,Screen.height/5),"100",style);
				//GUI.Box(new Rect(camera.WorldToScreenPoint(),Screen.height/PointMessageOffsetY,Screen.width/5,Screen.height/5),"Points"+"1",style);
	
		//if(!gamePlay)
		//{
		//	GUI.Box(new Rect(Screen.width/LevelMessageOffsetX,Screen.height/LevelMessageOffsetY,Screen.width/5,Screen.height/5),"(Right click to return to gameplay)",style);
		//}
			
	}
}
