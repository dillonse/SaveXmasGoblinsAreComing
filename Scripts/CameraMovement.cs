using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {
	
	private Transform player;
	public float limitXUp=350.0f;
	public float limitXDown=20.0f;
	public float limitYLeft=50.0f;
	public float limitYRight=-50.0f;
	public float cameraOffset=10.0f;
	public float cameraOffsetHor=20.0f;
	public float CameraSpeed=0.1f;
	
	public Transform CameraPoint1;
	public Transform CameraPoint2;
	public Transform CameraPoint3;
	
	public enum CameraAngle {Front,Back,Bird};
	public CameraAngle currentCameraAngle = CameraAngle.Front;
	
	
	public Transform PredictionTransform;
	
	void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void Start()
	{
		PredictionTransform.position=Vector3.zero;
		PredictionTransform.localScale = Vector3.one;
		PredictionTransform.rotation=transform.rotation;
		transform.parent = CameraPoint3;
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.Euler(Vector3.zero);
		currentCameraAngle = CameraAngle.Bird;
	}
	
	
	public void Strafe(string direction)
	{
		
		if(direction.CompareTo("Vertical")==0)
		{
			PredictionTransform.Rotate(-Vector3.right*Input.GetAxis("Vertical")*CameraSpeed);
			if(!(PredictionTransform.rotation.eulerAngles.x>limitXDown&&PredictionTransform.rotation.eulerAngles.x<limitXUp))
			{
				transform.Translate(cameraOffset*Vector3.forward);
				transform.Rotate(-Vector3.right*Input.GetAxis("Vertical")*CameraSpeed);
				transform.Translate(-cameraOffset*Vector3.forward);
			}
			else
			{
				PredictionTransform.Rotate(Vector3.right*Input.GetAxis("Vertical")*CameraSpeed);
			}
		}
		else if(direction.CompareTo("Horizontal")==0)
		{
			
			PredictionTransform.Rotate(-Vector3.up*Input.GetAxis("Horizontal")*CameraSpeed);
			if(!(PredictionTransform.rotation.eulerAngles.y>limitYRight&&PredictionTransform.rotation.eulerAngles.y<limitYLeft))
			{
				transform.Translate(cameraOffset*Vector3.forward);
				transform.Rotate(Vector3.up*Input.GetAxis("Horizontal")*CameraSpeed);
				transform.Translate(-cameraOffset*Vector3.forward);
			}
			else
			{
				PredictionTransform.Rotate(Vector3.up*Input.GetAxis("Horizontal")*CameraSpeed);
			}
		}

	}
	
	public void ChangeAngle(Transform angle)
	{
		transform.position = angle.position;
		transform.rotation = angle.rotation;
		
	}
	
	public void ChangeAngle(CameraAngle cameraAngle)
	{
		if(cameraAngle == CameraAngle.Front)
		{
			transform.parent = CameraPoint1;
			transform.localPosition= Vector3.zero;
			transform.localRotation = Quaternion.Euler(Vector3.zero);
			currentCameraAngle = CameraAngle.Front;
		}
		else if(cameraAngle == CameraAngle.Back)
		{
			transform.parent = CameraPoint2;
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.Euler(Vector3.zero);
			currentCameraAngle = CameraAngle.Back;
		}
		else if(cameraAngle == CameraAngle.Bird)
		{
			transform.parent = CameraPoint3;
			transform.localPosition= Vector3.zero;
			transform.localRotation = Quaternion.Euler(Vector3.zero);
			currentCameraAngle = CameraAngle.Bird;
		}
	}
	
	public void SwitchAngle()
	{
		if(currentCameraAngle == CameraAngle.Front)
		{
			ChangeAngle(CameraAngle.Back);
		}
		else
		{
			ChangeAngle(CameraAngle.Front);
		}
	}

}
