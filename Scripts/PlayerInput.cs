using UnityEngine;
using System.Collections;


public class PlayerInput : MonoBehaviour {

	private PlayerMovement playerMovement;
	private CameraMovement cameraMovement;
	private TargetingSystem targetingSystem;
	private PlayerAnimation playerAnimation;
	private AIStrategy aIStrategy;
	public bool EnableInput = false;
	public bool autoFocus = true;
	
	public enum PlayerMode {Santa,Elf} ;
	
	private PlayerMode playerMode=PlayerMode.Santa;
	
	public bool isPaused=false;
	public float InitTimeScale;
	
	public AudioSource audiosource;
	private GameObject ControllingElf;
	private GameObject ElfCamera;
	private GameObject MainCamera;
	
	void Awake()
	{
		playerMovement=GetComponent<PlayerMovement>();
		cameraMovement= GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMovement>();
		targetingSystem = GetComponent<TargetingSystem>();
		playerAnimation = GetComponent<PlayerAnimation>();
		aIStrategy = GameObject.FindGameObjectWithTag("AIStrategy").GetComponent<AIStrategy>();
		ElfCamera = GameObject.FindGameObjectWithTag("ElfCamera");
		MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		
	}
	
	void Start()
	{
		playerAnimation.Idle();
		playerMovement.Strafe(PlayerMovement.Direction.Forwards);
		InitTimeScale = Time.timeScale;
	}
	
	void Update()
	{
		if(EnableInput)
		{
				
			//MOVEMENT INPUT//
			/*if(Input.GetKey(KeyCode.W))
			{
				playerMovement.Move(PlayerMovement.Direction.Forwards);
				
				if(Input.GetKey(KeyCode.A))
				{
					playerMovement.Strafe(PlayerMovement.Direction.Left);
				}
				else if(Input.GetKey(KeyCode.D))
				{
					playerMovement.Strafe(PlayerMovement.Direction.Right);
				}
				playerAnimation.Run();
			}
			else if(Input.GetKey(KeyCode.S))
			{
				playerMovement.Move(PlayerMovement.Direction.Backwards);
				
				if(Input.GetKey(KeyCode.A))
				{
					playerMovement.Strafe(PlayerMovement.Direction.Left);
				}
				else if(Input.GetKey(KeyCode.D))
				{
					playerMovement.Strafe(PlayerMovement.Direction.Right);
				}
				playerAnimation.Run();
			}*/
			/*else*/ 
			if(Input.GetKey(KeyCode.A))
			{
				if((cameraMovement.currentCameraAngle== CameraMovement.CameraAngle.Front))
				{
					playerMovement.Move(PlayerMovement.Direction.Right);
				}
				else 
				{
					playerMovement.Move(PlayerMovement.Direction.Left);	
				}
				
				playerAnimation.Run();
				
			}
			else if(Input.GetKey(KeyCode.D))
			{
				if((cameraMovement.currentCameraAngle == CameraMovement.CameraAngle.Front))
				{
					playerMovement.Move(PlayerMovement.Direction.Left);
				}
				else
				{
					playerMovement.Move(PlayerMovement.Direction.Right);
				}
				playerAnimation.Run();
				
			}
	
			
			//FOCUS INPUT//
		/*	if(!autoFocus)
			{
				if(Input.GetMouseButtonDown(1))
				{
					aIStrategy.PlayerTarget(aIStrategy.targetPool);
				}
			 	if(Input.GetMouseButton(1)) 
				{
					targetingSystem.FocusOnEnemy();
				}
				else 
				{
					targetingSystem.UnFocusOnEnemy();
					aIStrategy.KillCrosshair();
					//playerMovement.Strafe();
					cameraMovement.Strafe("Vertical");
					//cameraMovement.Strafe("Horizontal");
		
				}
			}*/

			//FIRE INPUT//
	
			if(Input.GetMouseButtonDown(0))
			{
				playerAnimation.Fire();
				aIStrategy.ReCalculateNPCs();
				//aIStrategy.PlayerTarget(aIStrategy.targetPool);
				aIStrategy.PlayerTarget();
			}
			else if(Input.GetMouseButton(0))
			{
				if(autoFocus)
				{
					targetingSystem.FocusOnEnemy();
				}
			}
			else if(Input.GetMouseButtonUp(0))
			{
				if(autoFocus)
				{
					aIStrategy.UnPlayerTarget();
					targetingSystem.UnFocusOnEnemyPlayer();
				}
			}
			/*else if(Input.GetKeyDown(KeyCode.Space))
			{
				autoFocus = !autoFocus;
			}*/
			
			if(Input.GetKeyDown(KeyCode.Space))
			{
				if(cameraMovement.currentCameraAngle==CameraMovement.CameraAngle.Bird)
					cameraMovement.ChangeAngle(CameraMovement.CameraAngle.Front);
				else
					cameraMovement.ChangeAngle(CameraMovement.CameraAngle.Bird);
			}
			
			if(!Input.anyKey)
			{
				playerAnimation.Idle();
				playerMovement.Strafe(PlayerMovement.Direction.Forwards);
			}
			
			//STRATEGY INPUT
			/*if(Input.GetKeyDown(KeyCode.Alpha1))
			{
				//print("yoyo");
				aIStrategy.targetPool = EnemyAI.GoblinMode.Attacker;
				//aIStrategy.PlayerTarget(EnemyAI.GoblinMode.Attacker);
			}
			else if(Input.GetKeyDown(KeyCode.Alpha2))
			{
				aIStrategy.targetPool = EnemyAI.GoblinMode.Digger;
				//aIStrategy.PlayerTarget(EnemyAI.GoblinMode.Digger);
			}
			else if(Input.GetKeyDown(KeyCode.Alpha3))
			{
				aIStrategy.targetPool = EnemyAI.GoblinMode.Thief;
				//aIStrategy.PlayerTarget(EnemyAI.GoblinMode.RamDriver);
				
			}
			else if(Input.GetKeyDown(KeyCode.Alpha4))
			{
				aIStrategy.targetPool = EnemyAI.GoblinMode.RamDriver;
				//aIStrategy.PlayerTarget(EnemyAI.GoblinMode.Thief);
			}*/
			
			//HELPERS
			
			if(Input.GetKeyDown(KeyCode.F))
			{
				if(aIStrategy.SummonedAllies<aIStrategy.SummonedAlliesLimit)
				{
					playerMode=PlayerMode.Elf;
					ControllingElf= aIStrategy.SetAllies(1);
					aIStrategy.ReCalculateNPCs();
					aIStrategy.AssignTargets();
					
					aIStrategy.SummonedAllies++;
				}
			}
			
			if(Input.GetKeyDown(KeyCode.K))
			{
				if(ControllingElf)
				{
					//this.tag = "Ally";
					ControllingElf.GetComponent<TargetingSystem>().UnFocusOnEnemy();
					ControllingElf.GetComponent<AllyAI>().enabled=false;
					ControllingElf.GetComponent<NavMeshAgent>().enabled=false;
					ControllingElf.GetComponent<PlayerInputElf>().enabled=true;
					ControllingElf.tag = "Player";
					ElfCamera.transform.parent = ControllingElf.transform;
					MainCamera.GetComponent<Camera>().enabled =false;
					MainCamera.GetComponent<AudioListener>().enabled=false;
					ElfCamera.GetComponent<Camera>().enabled =true;
					ElfCamera.GetComponent<AudioListener>().enabled=true;
					ElfCamera.transform.localPosition = Vector3.up *1.2f-Vector3.forward*1.75f;
					ElfCamera.transform.localRotation = Quaternion.Euler(Vector3.zero);
					Screen.showCursor=false;
					this.enabled=false;
				}
			}
			

			
		}
			//PAUSE
			if(Input.GetKeyDown(KeyCode.Escape))
			{
				if(!isPaused)
				{
					Time.timeScale=0.0f;
					EnableInput = false;
					audiosource.Pause();
				}
				else
				{
					Time.timeScale = InitTimeScale;
					EnableInput = true;
					audiosource.Play();
				}
				
				isPaused = ! isPaused;
				
			}
		
	
			//JUMP INPUT//
		/*	if(Input.GetKeyDown(KeyCode.Space))
			{
				//playerMovement.Jump();
				//cameraMovement.ChangeAngle(cameraMovement.CameraPoint2);
				cameraMovement.SwitchAngle();
			}
			*/
		
	}
	
	
}
