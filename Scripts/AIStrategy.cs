using UnityEngine;
using System.Collections;

public class AIStrategy : MonoBehaviour {
	
	public enum AIMode {Enemy,Ally};
	public GameObject[] EnemyNPCS;
	public GameObject[] AllyNPCS;
	public System.Collections.Generic.List<GameObject> Attackers = new System.Collections.Generic.List<GameObject>();
	public System.Collections.Generic.List<GameObject> Diggers = new System.Collections.Generic.List<GameObject>();
	public System.Collections.Generic.List<GameObject> Thieves = new System.Collections.Generic.List<GameObject>();
	public System.Collections.Generic.List<GameObject> Drivers = new System.Collections.Generic.List<GameObject>();
	public AIMode aiMode = AIMode.Enemy;
	public GameObject Player;
	public GameObject playerTarget;
	
	public Material GoblinTarget;
	public Material GoblinNormal;
	
	
	public Camera MainCamera;
	
	public EnemyAI.GoblinMode targetPool=EnemyAI.GoblinMode.Attacker;
	
	public SaveSystem saveSystem;
	private bool SaveLock = true;
	
	public Transform InstPoint;
	public Transform AllyInstPoint;
	private float InstPointRadius=10.0f;
	
	
	public Object EnemyPrefab;
	public Object RamPrefab;
	public Object PlayerPrefab;
	public Object AllyPrefab;
	private GameObject ObjectPointer;
	private GameObject ObjectPointer2;
	
	public Transform Wall;
	public Transform Presents;
	public Transform Cave;
	public Transform CameraPointBE;
	
	
	public float LevelDuration=1000.0f;
	public float LevelTimer=0.0f;
	
	public int SummonedAlliesLimit=0;
	public int SummonedAllies = 0;
	
	public float WaveLimit=45f;
	public float WaveTimer=0.0f;
	public int WaveCount=1;
	
	public float DiggerLimit =10.0f;
	public float DiggerTimer=0.0f;
	
	public AudioSource audioSource;
	
	public void Awake()
	{
		EnemyNPCS = GameObject.FindGameObjectsWithTag("Enemy");
		AllyNPCS = GameObject.FindGameObjectsWithTag("Ally");
		Player = GameObject.FindGameObjectWithTag("Player");
		
	}
	
	public void Start()
	{
		ObjectPointer = new GameObject();
		ObjectPointer2 = new GameObject();
		Attackers.Clear();
		Diggers.Clear();
		Thieves.Clear();
		Drivers.Clear();
		
		foreach(GameObject r in EnemyNPCS)
		{
			if(r.GetComponent<EnemyAI>().goblinMode == EnemyAI.GoblinMode.Attacker)
			{
				
				Attackers.Add(r);
			}
			else if(r.GetComponent<EnemyAI>().goblinMode == EnemyAI.GoblinMode.Digger)
			{
				Diggers.Add(r);
			}
			else if(r.GetComponent<EnemyAI>().goblinMode == EnemyAI.GoblinMode.Thief)
			{
				Thieves.Add(r);
			}
			else
			{
				Drivers.Add(r);
			}
				
		}
		
	}
	
	
	
	void Update()
	{
		
		if(LevelTimer>=LevelDuration-2.0f)
		{
			if(SaveLock)
			{
				SaveLock=false;
				saveSystem.level++;
				saveSystem.Save();
				if(saveSystem.level<10)
					Application.LoadLevel("Loading");
				else
					Application.LoadLevel("Winner");
			}
		}

		
		if(WaveTimer>=WaveLimit)
		{
			WaveTimer=0.0f;
			WaveCount++;
			NewWave();
			ReCalculateNPCs();
			AssignTargets();
		}

		
		if(DiggerTimer>=DiggerLimit)
		{
			DiggerTimer = 0.0f;
			SetEnemies(0,1,0);
			ReCalculateNPCs();
			AssignTargets();
		}
		
		//if(audioSource.isPlaying)
		{
			//print(LevelDuration);
//			print(LevelTimer);
			LevelTimer+=Time.deltaTime;
			WaveTimer+=Time.deltaTime;
			DiggerTimer+=Time.deltaTime;
		}
		

		
	}
	
	public void AssignTargets()
	{
		int counter=0;
		foreach(GameObject r in Attackers)
		{
			if(counter<AllyNPCS.Length)
				r.GetComponent<TargetingSystem>().target= AllyNPCS[counter].GetComponent<TargetingSystem>().targetee;
			else
				r.GetComponent<TargetingSystem>().target= Player.GetComponent<TargetingSystem>().targetee;
			counter++; 
		}
		
		counter = 0;
		foreach(GameObject r in AllyNPCS)
		{
			if(counter<Attackers.Count)
			{
				r.GetComponent<TargetingSystem>().target = Attackers[counter].GetComponent<TargetingSystem>().targetee;
			}
			else if(counter< (Diggers.Count+Attackers.Count))
			{
				r.GetComponent<TargetingSystem>().target = Diggers[counter-Attackers.Count].GetComponent<TargetingSystem>().targetee;
			}
			else if(counter<Thieves.Count+Attackers.Count+Diggers.Count)
			{
				r.GetComponent<TargetingSystem>().target = Thieves[counter-Attackers.Count-Diggers.Count].GetComponent<TargetingSystem>().targetee;
				
			}
			else if(EnemyNPCS.Length>0)
			{
				r.GetComponent<TargetingSystem>().target = EnemyNPCS[0].GetComponent<TargetingSystem>().targetee;
				counter=0;
			}
			
			counter++;
		}
	}
	
	public void AssignTargets(string Type)
	{
		int counter=0;
		if(Type.CompareTo("Enemy")==0)
		{
			foreach(GameObject r in Attackers)
			{
				if(counter<AllyNPCS.Length)
					r.GetComponent<TargetingSystem>().target= AllyNPCS[counter].GetComponent<TargetingSystem>().targetee;
				else
					r.GetComponent<TargetingSystem>().target= Player.GetComponent<TargetingSystem>().targetee;
				counter++; 
			}
		}
		else if(Type.CompareTo("Ally")==0)
		{
			counter = 0;
			foreach(GameObject r in AllyNPCS)
			{
				if(counter<Attackers.Count)
				{
					r.GetComponent<TargetingSystem>().target = Attackers[counter].GetComponent<TargetingSystem>().targetee;
				}
				else if(counter< (Diggers.Count+Attackers.Count))
				{
					r.GetComponent<TargetingSystem>().target = Diggers[counter-Attackers.Count].GetComponent<TargetingSystem>().targetee;
				}
				else if(counter<Thieves.Count+Attackers.Count+Diggers.Count)
				{
					r.GetComponent<TargetingSystem>().target = Thieves[counter-Attackers.Count-Diggers.Count].GetComponent<TargetingSystem>().targetee;
					
				}
				else if(EnemyNPCS.Length>0)
				{
					r.GetComponent<TargetingSystem>().target = EnemyNPCS[0].GetComponent<TargetingSystem>().targetee;
					counter=0;
				}
				
				r.collider.enabled=true;
				
				counter++;
			}
		}
	}
	
	public void ReCalculateNPCs()
	{
			
		EnemyNPCS = GameObject.FindGameObjectsWithTag("Enemy");
		
		if(EnemyNPCS.Length>0)
		{
//			print("Found "+EnemyNPCS.Length+ "Enemies");
			AllyNPCS = GameObject.FindGameObjectsWithTag("Ally");
			Player = GameObject.FindGameObjectWithTag("Player");
			
			Attackers.Clear();
			Diggers.Clear();
			Thieves.Clear();
			Drivers.Clear();
			
			foreach(GameObject r in EnemyNPCS)
			{
				if(r.GetComponent<EnemyAI>().goblinMode == EnemyAI.GoblinMode.Attacker)
				{
					
					Attackers.Add(r);
				}
				else if(r.GetComponent<EnemyAI>().goblinMode == EnemyAI.GoblinMode.Digger)
				{
					Diggers.Add(r);
				}
				else if(r.GetComponent<EnemyAI>().goblinMode == EnemyAI.GoblinMode.Thief)
				{
					Thieves.Add(r);
				}
				else
				{
					Drivers.Add(r);
				}
			}
		}
		/*else
		{
			if(SaveLock)
			{
				SaveLock=false;
				saveSystem.level++;
				saveSystem.Save();
				if(saveSystem.level<10)
					Application.LoadLevel("Loading");
				else
					Application.LoadLevel("Winner");
			}
		}*/
		

		
	}
	
	public void UnPlayerTarget()
	{
		if(playerTarget)
		{
			//playerTarget.GetComponent<TargetingSystem>().Crosshair.renderer.enabled=false;
			Transform[] AnimationStates = playerTarget.GetComponentsInChildren<Transform>();
			foreach(Transform r in AnimationStates)
			{
				if(r.name=="Box001")
				r.GetComponentInChildren<Renderer>().sharedMaterial = GoblinNormal;
			}
		}
		playerTarget=null;
			
	}
	
	public void PlayerTarget()
	{	
		float nearestDistance= Mathf.Infinity;
		
		/*if(playerTarget)
		{
			//playerTarget.GetComponent<TargetingSystem>().Crosshair.renderer.enabled=false;
			Transform[] AnimationStates = playerTarget.GetComponentsInChildren<Transform>();
			foreach(Transform r in AnimationStates)
			{
				if(r.name=="Box001")
				r.GetComponentInChildren<Renderer>().sharedMaterial = GoblinNormal;
			}
		}
		*/
		ReCalculateNPCs();
		foreach(GameObject r in EnemyNPCS)
		{
			Vector3 ProjectileFromCamera = MainCamera.WorldToScreenPoint(r.transform.position);
			if(Vector3.Distance(Input.mousePosition,ProjectileFromCamera)<nearestDistance)
			{
				nearestDistance = Vector3.Distance(Input.mousePosition,ProjectileFromCamera);
				playerTarget = r;
				
			}
		}
		if(playerTarget)
		{
			Player.GetComponent<TargetingSystem>().target = playerTarget.GetComponent<TargetingSystem>().targetee;
			//playerTarget.GetComponent<TargetingSystem>().Crosshair.renderer.enabled=true;
		 	Transform[] AnimationStates = playerTarget.GetComponentsInChildren<Transform>();
			foreach(Transform r in AnimationStates)
			{
				//print(r.name);
				if(r.name=="Box001")
				r.GetComponentInChildren<Renderer>().sharedMaterial = GoblinTarget;
			}
		}
		
		
	}
	
	public void PlayerTarget(EnemyAI.GoblinMode targetType)
	{
		float nearestDistance= Mathf.Infinity;
		
		if(playerTarget)
			playerTarget.GetComponent<TargetingSystem>().Crosshair.renderer.enabled=false;
		
		ReCalculateNPCs();
		if(targetType == EnemyAI.GoblinMode.Attacker)
		{
			foreach(GameObject r in Attackers)
			{
				if(r)
				if(Vector3.Distance(Player.transform.position,r.transform.position)<nearestDistance)
				{
					nearestDistance = Vector3.Distance(Player.transform.position,r.transform.position);
					playerTarget = r;
					
				}
			}
		}
		else if(targetType == EnemyAI.GoblinMode.Digger)
		{
			foreach(GameObject r in Diggers)
			{
				if(r)
				if(Vector3.Distance(Player.transform.position,r.transform.position)<nearestDistance)
				{
					nearestDistance = Vector3.Distance(Player.transform.position,r.transform.position);
					playerTarget = r;
					
				}
			}
		}
		else if(targetType == EnemyAI.GoblinMode.Thief)
		{
			foreach(GameObject r in Thieves)
			{
				if(r)
				if(Vector3.Distance(Player.transform.position,r.transform.position)<nearestDistance)
				{
					nearestDistance = Vector3.Distance(Player.transform.position,r.transform.position);
					playerTarget = r;
					
				}
			}
		}
		else if(targetType == EnemyAI.GoblinMode.RamDriver)
		{
			foreach(GameObject r in Drivers)
			{
				if(r)
				if(Vector3.Distance(Player.transform.position,r.transform.position)<nearestDistance)
				{
					nearestDistance = Vector3.Distance(Player.transform.position,r.transform.position);
					playerTarget = r;
					
				}
			}
			
		}
		if(!playerTarget)
		{
			PlayerTarget();
		}
		else
		{
			Player.GetComponent<TargetingSystem>().target = playerTarget.GetComponent<TargetingSystem>().targetee;
			playerTarget.GetComponent<TargetingSystem>().Crosshair.renderer.enabled=true;
		}
		
		
	}
	
	public void KillCrosshair()
	{
		if(playerTarget)
			playerTarget.GetComponent<TargetingSystem>().Crosshair.renderer.enabled=false;
	}
	

	
	public void SetEnemies(int AttackersCount,int DiggersCount,int RamsCount,bool followCamera=false)
	{	
		for(int i=0;i<DiggersCount;i++)
		{
			ObjectPointer= Instantiate(EnemyPrefab,InstPoint.position,Quaternion.Euler(Random.Range(0f,350f)*Vector3.up)) as GameObject ;
			ObjectPointer.GetComponent<EnemyAI>().Wall = Wall;
			ObjectPointer.GetComponent<EnemyAI>().Presents = Presents;
			ObjectPointer.GetComponent<EnemyAI>().Cave = Cave;
			ObjectPointer.GetComponent<EnemyAI>().goblinMode = EnemyAI.GoblinMode.Digger;
			ObjectPointer.GetComponent<EnemyAI>().Awake();
			if(followCamera)
				Player.GetComponent<TargetingSystem>().target = ObjectPointer.GetComponent<TargetingSystem>().targetee;
		}
		for(int i=0;i<RamsCount;i++)
		{
			ObjectPointer= Instantiate(RamPrefab,InstPoint.position-Vector3.right*50f,InstPoint.rotation) as GameObject ;
			ObjectPointer.GetComponent<RamAI>().Wall = Wall;
			ObjectPointer.GetComponent<RamAI>().Awake();
			
			ObjectPointer2 = Instantiate(EnemyPrefab,ObjectPointer.GetComponent<RamAI>().Goblin1Transform.position,ObjectPointer.GetComponent<RamAI>().Goblin1Transform.rotation) as GameObject ;
			ObjectPointer2.GetComponent<EnemyAI>().Wall=Wall;
			ObjectPointer2.GetComponent<EnemyAI>().Cave = Cave;
			ObjectPointer2.GetComponent<EnemyAI>().Presents = Presents;
			ObjectPointer2.GetComponent<EnemyAI>().goblinMode = EnemyAI.GoblinMode.RamDriver;
			ObjectPointer2.GetComponent<EnemyAI>().ramAI = ObjectPointer.GetComponent<RamAI>();
			ObjectPointer.GetComponent<RamAI>().Goblin1 = ObjectPointer2;
			
			ObjectPointer2.transform.parent = ObjectPointer.transform;
			ObjectPointer2.GetComponent<EnemyAI>().Awake();
			
			ObjectPointer2 = Instantiate(EnemyPrefab,ObjectPointer.GetComponent<RamAI>().Goblin2Transform.position,ObjectPointer.GetComponent<RamAI>().Goblin2Transform.rotation) as GameObject ;
			ObjectPointer2.GetComponent<EnemyAI>().Wall=Wall;
			ObjectPointer2.GetComponent<EnemyAI>().Cave = Cave;
			ObjectPointer2.GetComponent<EnemyAI>().Presents = Presents;
			ObjectPointer2.GetComponent<EnemyAI>().goblinMode = EnemyAI.GoblinMode.RamDriver;
			ObjectPointer2.GetComponent<EnemyAI>().ramAI = ObjectPointer.GetComponent<RamAI>();
			ObjectPointer.GetComponent<RamAI>().Goblin2 = ObjectPointer2;
			
			if(followCamera)
				Player.GetComponent<TargetingSystem>().target = ObjectPointer2.GetComponent<TargetingSystem>().targetee;
			
			ObjectPointer2.transform.parent = ObjectPointer.transform;
			ObjectPointer2.GetComponent<EnemyAI>().Awake();
			
		}
		for(int i =0;i<AttackersCount;i++)
		{
			ObjectPointer= Instantiate(EnemyPrefab,InstPoint.position,Quaternion.Euler(Random.Range(0f,350f)*Vector3.up)) as GameObject ;
			ObjectPointer.GetComponent<EnemyAI>().Wall = Wall;
			ObjectPointer.GetComponent<EnemyAI>().Presents = Presents;
			ObjectPointer.GetComponent<EnemyAI>().Cave = Cave;
			ObjectPointer.GetComponent<EnemyAI>().goblinMode = EnemyAI.GoblinMode.Attacker;
			ObjectPointer.GetComponent<EnemyAI>().Awake();
			if(followCamera)
				Player.GetComponent<TargetingSystem>().target = ObjectPointer.GetComponent<TargetingSystem>().targetee;
		}
		
		
		
	}
	
	public GameObject SetAllies(int AlliesCount)
	{
		for(int i=0;i<AlliesCount;i++)
		{
			ObjectPointer= Instantiate(AllyPrefab,AllyInstPoint.position+Vector3.right*Random.Range(0,InstPointRadius)+Vector3.forward*Random.Range(-InstPointRadius,InstPointRadius),Quaternion.Euler(InstPoint.rotation.eulerAngles+Vector3.up*Random.Range(0,360))) as GameObject ;
		}
		if(AlliesCount>0)
			return ObjectPointer;
		else return null;
	}
	
	public void NewWave()
	{
		if(saveSystem.level==0)
		{
			if(WaveCount == 1)
			{
				SetEnemies(1,1,0,true);
				SummonedAlliesLimit = 0;
			}
			else if(WaveCount == 2)
			{
				SetEnemies(1,2,0);
				SummonedAlliesLimit = 0;
			}
			else if(WaveCount == 3)
			{
				SummonedAlliesLimit = 1;
				SetEnemies(2,2,1);
			}
			
		}
		else if(saveSystem.level==1)
		{
			if(WaveCount == 1)
			{
				SetEnemies(1,2,0,true);
				SummonedAlliesLimit = 4;
			}
			else if(WaveCount == 2)
			{
				SetEnemies(2,2,0);
				SummonedAlliesLimit = 1;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(4,4,1);
				SummonedAlliesLimit = 2;
			}
		}
		else if(saveSystem.level==2)
		{
			if(WaveCount == 1)
			{
				SetEnemies(2,3,0,true);
				SummonedAlliesLimit = 1;
			}
			else if(WaveCount==2)
			{
				SetEnemies(3,4,0);
				SummonedAlliesLimit = 2;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(5,3,1);
				SummonedAlliesLimit = 2;
			}
		}
		else if(saveSystem.level == 3)
		{
			if(WaveCount == 1)
			{
				SetEnemies(4,5,0,true);
				SummonedAlliesLimit = 2;
			}
			else if(WaveCount == 2)
			{
				SetEnemies(7,4,0);
				SummonedAlliesLimit = 4;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(7,4,1);
				SummonedAlliesLimit = 4;
			}
		}
		else if(saveSystem.level == 4)
		{
			if(WaveCount == 1)
			{
				SetEnemies(2,4,0,true);
				SummonedAlliesLimit = 1;
			}
			else if(WaveCount == 2)
			{
				SetEnemies(5,5,0);
				SummonedAlliesLimit = 3;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(5,5,1);
				SummonedAlliesLimit = 4;
			}
		}
		else if(saveSystem.level == 5)
		{
			if(WaveCount == 1)
			{
				SetEnemies(6,7,0,true);
				SummonedAlliesLimit = 3;
			}
			else if(WaveCount == 2)
			{
				SetEnemies(7,7,0);
				SummonedAlliesLimit = 4;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(8,8,1);
				SummonedAlliesLimit = 5;
			}
		}
		else if(saveSystem.level==6)
		{
			if(WaveCount == 1)
			{
				SetEnemies(2,4,0,true);
				SummonedAlliesLimit = 1;
			}
			else if(WaveCount==2)
			{
				SetEnemies(2,4,0);
				SummonedAlliesLimit = 1;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(8,7,1);
				SummonedAlliesLimit = 5;
			}
		}
		else if(saveSystem.level==7)
		{
			if(WaveCount == 1)
			{
				SetEnemies(5,5,0,true);
				SummonedAlliesLimit = 3;
			}
			else if(WaveCount == 2)
			{
				SetEnemies(6,5,0);	
				SummonedAlliesLimit = 3;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(6,10,1);
				SummonedAlliesLimit = 3;
			}
		}
		else if(saveSystem.level == 8)
		{
			if(WaveCount == 1)
			{
				SetEnemies(4,5,0,true);
				SummonedAlliesLimit = 2;
			}
			else if(WaveCount == 2)
			{
				SetEnemies(8,5,0);
				SummonedAlliesLimit = 4;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(10,10,1);
				SummonedAlliesLimit = 5;
			}
		}
		else if(saveSystem.level == 9)
		{
			if(WaveCount == 1)
			{
				SetEnemies(10,7,1,true);
				SummonedAlliesLimit = 5;
			}
			else if(WaveCount == 2)
			{
				SetEnemies(14,12,3);
				SummonedAlliesLimit = 7;
			}
			else if(WaveCount == 3)
			{
				SetEnemies(18,10,3);
				SummonedAlliesLimit = 10;
			}
			
		}
	}
	

	
}
