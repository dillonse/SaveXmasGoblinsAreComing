using UnityEngine;
using System.Collections;

public class DamageSystem : MonoBehaviour {

	public float Life = 10f;
	public float Damage = 2.5f;
	
	public AIStrategy aIStrategy;
	
//	private SaveSystem saveSystem;
	
	public enum Type {Ally,Enemy};
	

	
	void Awake()
	{
		aIStrategy = GameObject.FindGameObjectWithTag("AIStrategy").GetComponent<AIStrategy>();
//		saveSystem = GameObject.FindGameObjectWithTag("SaveSystem").GetComponent<SaveSystem>();
	}
	
	void OnCollisionEnter(Collision other)
	{
		if(other.collider.gameObject.CompareTag("Projectile"))
		{
			Life-=Damage;
		}
		if(Life<=0f)
		{
			
			particleEmitter.emit=true;
			Life=0;
			//collider.enabled=false;
			if(!gameObject.CompareTag("Player"))
			{
				gameObject.tag="Untagged";
				aIStrategy.ReCalculateNPCs();
				aIStrategy.AssignTargets();
			}

			foreach(Transform r in transform)
			{
				if(r.name=="CameraPointBirdEyeEnemies")
					r.parent=null;
			}

		}
	}
}
