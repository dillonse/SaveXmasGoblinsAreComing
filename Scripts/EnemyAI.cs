using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
	
	
	private TargetingSystem targetingSystem;
	private NavMeshAgent navMeshAgent;
	public float fireRate = 0.5f;
	private float fireRateTimer=0.0f;
	public float distanceLimit=25.0f;
	
	public float DigRate = 0.5f;
	private float DigRateTimer=0.0f;
	//public float AILoop = -3.0f;
	//private float AILoopTimer=0.0f;
	
	public enum GoblinMode {Attacker,Digger,Thief,RamDriver};
	public enum AnimationMode {Throw,Dig,Run,Carry};
	public GoblinMode goblinMode = GoblinMode.Attacker;
	
	public Renderer RunRenderer;
	public Renderer CarryRenderer;
	public Renderer ThrowRenderer;
	public Renderer DigRenderer;
	
	public Animation RunAnimation;
	public Animation CarryAnimation;
	public Animation ThrowAnimation;
	public Animation DigAnimation;
	
	public GUIElements guiElements;
	
	public Transform Wall;
	private float WallLeftLimit;
	private float WallRightLimit;
	public Vector3 WallTargetPos;
	public Transform Presents;
	public Transform Cave;
	public bool hasPresent=false;
	
	public RamAI ramAI;
	private  WallAI wallAI ;
	
	public bool insideWall = false;
	public bool BeforeWall = false;
	
	
	public Renderer PresentRenderer;
	
	public Texture[] PresentsTexture;
	
	private bool SaveLock=true;
	private SaveSystem saveSystem;

	public void Awake()
	{
		targetingSystem = GetComponent<TargetingSystem>();
		navMeshAgent = GetComponent<NavMeshAgent>();
		if(Wall)
		wallAI = Wall.GetComponent<WallAI>();
		
		PresentRenderer.material.mainTexture = PresentsTexture[Random.Range(0,PresentsTexture.Length-1)];
		
		saveSystem = GameObject.FindGameObjectWithTag("SaveSystem").GetComponent<SaveSystem>();
		guiElements = GameObject.Find("GUICamera").GetComponent<GUIElements>();
		
	}
	
	
	void Start()
	{
		SwitchAnimation(AnimationMode.Run);
		if(goblinMode==GoblinMode.Attacker)
		{
			GetComponent<DamageSystem>().Life=1;
			GetComponent<DamageSystem>().Damage=1;
		}
		else if(goblinMode == GoblinMode.Digger)
		{
			GetComponent<DamageSystem>().Life=2;
			GetComponent<DamageSystem>().Damage=1;
		}
		else if(goblinMode == GoblinMode.RamDriver)
		{
			GetComponent<DamageSystem>().Life=3;
			GetComponent<DamageSystem>().Damage=1;
		}
		else if(goblinMode == GoblinMode.Thief)
		{
			GetComponent<DamageSystem>().Life=2;
			GetComponent<DamageSystem>().Damage=1;
		}
		
		WallLeftLimit =Wall.position.z - (Wall.collider.bounds.size.z/2);
		WallRightLimit =Wall.position.z + (Wall.collider.bounds.size.z/2);
		WallTargetPos = Wall.position.y*Vector3.up+Random.Range(WallLeftLimit,WallRightLimit)*Vector3.forward + Vector3.right*(Wall.position.x+Wall.collider.bounds.size.x/2);
//		print(WallTargetPos);
	}
	
	void FixedUpdate()
	{
		//if(AILoopTimer>AILoop)
		{
//			AILoopTimer=0.0f;
			if(goblinMode==GoblinMode.Attacker)
				Attack();
			else if(goblinMode == GoblinMode.Digger)
				Dig();
			else if(goblinMode == GoblinMode.Thief)
				Steal();
			else if(goblinMode == GoblinMode.RamDriver)
				Drive();
		}
		//AILoopTimer+=Time.fixedDeltaTime;
	}
	
	void Attack()
	{
		
		if(targetingSystem.target)
		{
			navMeshAgent.enabled=true;
			navMeshAgent.SetDestination(targetingSystem.target.position);
			navMeshAgent.stoppingDistance=20f;
			
			if(!ThrowAnimation.isPlaying)
			{
				SwitchAnimation(AnimationMode.Run);
				targetingSystem.UnFocusOnEnemy();
			}
			if(Vector2.Distance(new Vector2(targetingSystem.target.position.x,targetingSystem.target.position.z),new Vector2(transform.position.x,transform.position.z))<distanceLimit)
			{
				targetingSystem.FocusOnEnemy();
				if(fireRateTimer>1f)
				{	
					SwitchAnimation(AnimationMode.Throw);
					fireRateTimer=0.0f;
				}
				else
				{
					fireRateTimer+=Time.deltaTime*fireRate;
				}
			}
		}
		else
		{
			targetingSystem.UnFocusOnEnemy();
			goblinMode=GoblinMode.Digger;
		}
	}
	
	void Dig()
	{
		if(wallAI)
		if(!wallAI.isDestroyed)
		{
			navMeshAgent.SetDestination(WallTargetPos);
			navMeshAgent.stoppingDistance=1f;
			if(Vector3.Distance(transform.position,WallTargetPos)<5f)
			{
				SwitchAnimation(AnimationMode.Dig);
				if(DigRateTimer>DigRate)
				{
					wallAI.Life-=1.0f;
					DigRateTimer=0.0f;
				}
				DigRateTimer+=Time.deltaTime;
				transform.LookAt(Vector3.right*WallTargetPos.x+Vector3.forward*transform.position.z+Vector3.up*transform.position.y);
			}
			else
			{
				SwitchAnimation(AnimationMode.Run);
			}
		}
		else
		{
			goblinMode = GoblinMode.Thief;
		}
	}
	
	void Drive()
	{
		SwitchAnimation(AnimationMode.Dig);
		//navMeshAgent.SetDestination(transform.position);
		navMeshAgent.enabled=false;
	}
	
	void Steal()
	{
		navMeshAgent.stoppingDistance=0.0f;
		if(!hasPresent)
		{	
			navMeshAgent.SetDestination(Presents.position);
			SwitchAnimation(AnimationMode.Run);
			if(Vector3.Distance(transform.position,Presents.position)<8f)
			{
				hasPresent=true;
				PresentRenderer.enabled=true;
				
			}
		}
		else
		{
			navMeshAgent.SetDestination(Cave.position);
			SwitchAnimation(AnimationMode.Carry);
		}
	}
	
	
	void SwitchAnimation(AnimationMode animation)
	{
		if(animation == AnimationMode.Run)
		{
			RunAnimation.enabled=true;
			DigAnimation.enabled=false;
			CarryAnimation.enabled=false;
			ThrowAnimation.enabled=false;
			
			RunRenderer.enabled=true;
			CarryRenderer.enabled=false;
			DigRenderer.enabled=false;
			ThrowRenderer.enabled=false;
		}
		else if(animation == AnimationMode.Carry)
		{
			CarryAnimation.enabled=true;
			RunAnimation.enabled=false;
			DigAnimation.enabled=false;
			ThrowAnimation.enabled=false;
			
			CarryRenderer.enabled=true;
			DigRenderer.enabled=false;
			RunRenderer.enabled=false;
			ThrowRenderer.enabled=false; 
		}
		else if(animation == AnimationMode.Dig)
		{
			DigAnimation.enabled=true;
			RunAnimation.enabled=false;
			CarryAnimation.enabled=false;
			ThrowAnimation.enabled=false;
			
			DigRenderer.enabled=true;
			RunRenderer.enabled=false;
			CarryRenderer.enabled=false;
			ThrowRenderer.enabled=false;
		}
		else if(animation == AnimationMode.Throw)
		{
			ThrowAnimation.enabled=true;
			ThrowAnimation.Stop();
			ThrowAnimation.Rewind();
			ThrowAnimation.Play();
			
			RunAnimation.enabled=false;
			DigAnimation.enabled=false;
			CarryAnimation.enabled=false;
			
			ThrowRenderer.enabled=true;
			RunRenderer.enabled=false;
			CarryRenderer.enabled=false;
			DigRenderer.enabled=false;
		}
	}
	
	void OnDestroy()
	{
		guiElements.HitGoblin = new Vector3(transform.position.x,transform.position.y,transform.position.z);
		/*if(goblinMode == GoblinMode.RamDriver)
		{
			if(SaveLock)
			{
				SaveLock=false;
				saveSystem.level++;
				saveSystem.Save();
				if(saveSystem.level<10)
					Application.LoadLevel("Loading");
				else
					Application.LoadLevel("Winner");
			}	
		}*/
	}
	

}
