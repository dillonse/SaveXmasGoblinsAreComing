using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	
	public enum Direction {Left,Right,Forwards,Backwards}; 
	
	public float speed=15.0f;
	private float backwardSpeed;
	
	public float strafeSpeed=1.0f;
	
	public float jumpSpeed=500.0f;
	private bool isGrounded = false;
	
	public float sideWalkSpeed=15.0f;
	
	public float MoveLimitLeft = 80f;
	
	public float MoveLimitRight = 120f;
	
	public bool IsGrounded()
	{
		return isGrounded;
	}
	
	public void Start()
	{
		transform.rotation = Quaternion.Euler(Vector3.zero);
		backwardSpeed=speed/2;
	}
	
	void OnCollisionEnter(Collision other)
	{
		if(other.collider.gameObject.tag== "Walkable")
			isGrounded=true;
	}
	
	public void Move(Direction direction)
	{
		if(direction == Direction.Left && transform.position.z<MoveLimitLeft)
		{
			transform.rotation = Quaternion.Euler(Vector3.zero);
			transform.Translate(Vector3.forward*Time.deltaTime*speed);
		}
		else if(direction == Direction.Right && transform.position.z>MoveLimitRight)
		{
			transform.rotation = Quaternion.Euler(Vector3.up*180f);
			transform.Translate(Vector3.forward*Time.deltaTime*speed);
		}
//		print(transform.position);
		
	}
	
	public void Strafe(Direction direction)
	{
		
		if(direction == Direction.Forwards)
		{
			transform.rotation = Quaternion.Euler(Vector3.up*90f);
		}
	}
	
	public void SideWalk(Direction direction)
	{
		if(direction==Direction.Left)
		{
			
			transform.Translate(-Vector3.right*sideWalkSpeed*Time.deltaTime);
		}
		else
		{
			transform.Translate(Vector3.right*sideWalkSpeed*Time.deltaTime);
		}
	}
	
	public void Jump()
	{
		if(isGrounded)
		{
			rigidbody.AddForce(Vector3.up*jumpSpeed);
			isGrounded=false;
		}
	}
	
	public void Strafe()
	{
		transform.Rotate(Vector3.up*Input.GetAxis("Horizontal")*strafeSpeed);
	}
	
}
