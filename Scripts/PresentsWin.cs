using UnityEngine;
using System.Collections;

public class PresentsWin : MonoBehaviour {

	public Texture[] textures;
	public Object PresentPrefab;
	
	public float Radius;
	public float FireRate=0.5f;
	private float FireRateTimer=0.0f;
	// Update is called once per frame
	void Awake () 
	{
		//renderer.material.mainTexture = textures[Random.Range(0,textures.Length-1)];
	}
	
	void Update()
	{
		if(FireRateTimer>FireRate)
		{
			FireRateTimer=0.0f;
			Destroy(Instantiate(PresentPrefab,transform.position+Vector3.right*Random.Range(-Radius,Radius),Quaternion.Euler(Vector3.right*(-90f))) as GameObject,1f);
		}
		FireRateTimer+=Time.deltaTime;
	
	}
}
