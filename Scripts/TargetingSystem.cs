using UnityEngine;
using System.Collections;

public class TargetingSystem : MonoBehaviour {
	
	public Transform target;
	public Transform targetee;
	public Object projectile;
	private GameObject projectileReference;
	public Transform projectileTransform;
	private float projectileLifetime=4.0f;
	public float projectileSpeed=100.0f;
	private SmoothLookAt smoothLookAt;
	GameObject temp;
	
	public GameObject Crosshair;
	
	private CameraMovement cameraMovement;
	
	void Awake()
	{
		smoothLookAt = GetComponent<SmoothLookAt>();
		if(gameObject.CompareTag("Player"))
		{
			cameraMovement = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMovement>();
		}
	}
	
	void Start()
	{
		temp=new GameObject();
	}
	
	void Update()
	{
		if(transform.CompareTag("Player"))
		{
			if(target)
			{
				if(cameraMovement)
				if(cameraMovement.currentCameraAngle == CameraMovement.CameraAngle.Bird )
					cameraMovement.transform.LookAt(target.position);
			}
			else
			{
				if(cameraMovement)
				if(cameraMovement.currentCameraAngle == CameraMovement.CameraAngle.Bird )
					cameraMovement.transform.LookAt(transform.position);
			}
		}
	}
	
	public void FocusOnEnemy()
	{
		
		if(target)
		{
			if(transform.CompareTag("Player"))
			{
				/*if(target.parent.GetComponent<EnemyAI>().insideWall)
				{
					cameraMovement.ChangeAngle(CameraMovement.CameraAngle.Back);
				}
				else if(target.parent.GetComponent<EnemyAI>().BeforeWall)
				{
					cameraMovement.ChangeAngle(CameraMovement.CameraAngle.Front);
				}
				else
				{
					cameraMovement.ChangeAngle(CameraMovement.CameraAngle.Bird);
					//camera.transform.LookAt(target.position);
					//cameraMovement.CameraPoint3.parent = target;
				}*/
			}
			projectileTransform.LookAt(target.position);
			temp.transform.position=Vector3.up*transform.position.y+Vector3.right*target.position.x+Vector3.forward*target.position.z;
			smoothLookAt.target=temp.transform;
			smoothLookAt.enabled=true;
		}
	}
	
	public void ResetFocus()
	{
		smoothLookAt.enabled=true;
		smoothLookAt.target=transform;
	}
	
	public void UnFocusOnEnemy()
	{
		smoothLookAt.enabled=false;
		projectileTransform.localRotation=Quaternion.Euler(Vector3.zero);
	}
	
	public void UnFocusOnEnemyPlayer()
	{
		smoothLookAt.enabled=false;
		projectileTransform.localRotation=Quaternion.Euler(Vector3.zero);
		target=null;
		
	}
	
	public void Fire()
	{
		Destroy(projectileReference=Instantiate(projectile,projectileTransform.position,projectileTransform.rotation) as GameObject,projectileLifetime);
		projectileReference.rigidbody.AddForce(projectileTransform.forward*projectileSpeed);	
	}
	
	void OnDestroy()
	{
		Destroy(temp);
	}
}
