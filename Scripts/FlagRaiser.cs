using UnityEngine;
using System.Collections;

public class FlagRaiser : MonoBehaviour {

	
	public enum Flag {InsideWall,BeforeWall,InsideCave}
	public Flag flag = Flag.BeforeWall;
	
	private AIStrategy aiStrategy;
	
	private CameraMovement cameraMovement;
	//private PlayerInput playerInput;
	
	public SaveSystem saveSystem;
	
	public GUIElements guiElements;
	
	public void Awake()
	{
		aiStrategy = GameObject.FindGameObjectWithTag("AIStrategy").GetComponent<AIStrategy>();
		cameraMovement = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMovement>();
		//playerInput = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInput>();
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Enemy"))
		{
			if(flag == Flag.InsideWall)
			{
				other.GetComponent<EnemyAI>().insideWall=true;
			}
			else if(flag == Flag.BeforeWall)
			{
			}
			else if(flag == Flag.InsideCave)
			{
				if(other.GetComponent<EnemyAI>().hasPresent)
				{
					other.gameObject.tag = "Untagged";
					saveSystem.Presents--;
					Destroy(other.gameObject);
					aiStrategy.ReCalculateNPCs();
					
					if(saveSystem.Presents<=0)
					{
						Application.LoadLevel("Loser");
					}
				}
			}
		}
	}
	
	void OnTriggerExit(Collider other)
	{
		if(other.CompareTag("Enemy"))
		{
			if(flag == Flag.InsideWall)
			{
				other.GetComponent<EnemyAI>().insideWall=false;
			}
			else if(flag == Flag.BeforeWall)
			{
				other.GetComponent<EnemyAI>().BeforeWall =false;
			}
		}
	}
}
