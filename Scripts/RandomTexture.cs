using UnityEngine;
using System.Collections;

public class RandomTexture : MonoBehaviour {
	
	public Texture[] textures;
	// Use this for initialization
	void Start () 
	{
		renderer.material.mainTexture = textures[Random.Range(0,textures.Length-1)];	
	}
	
}
