using UnityEngine;
using System.Collections;

public class AllyAI : MonoBehaviour {

	public enum AnimationMode {Throw,Run};
		
	private TargetingSystem targetingSystem;
	private NavMeshAgent navMeshAgent;
	public float fireRate = 0.5f;
	private float fireRateTimer=0.0f;
	public float distanceLimit=25.0f;
	public float AILoop=-1f;
	private float AILoopTimer=0.0f;
	
	public Renderer RunRenderer;
	public Renderer ThrowRenderer;
	
	public Animation RunAnimation;
	public Animation ThrowAnimation;
	
	//private AIStrategy aIStrategy;
	
	void Awake()
	{
		targetingSystem = GetComponent<TargetingSystem>();
		navMeshAgent = GetComponent<NavMeshAgent>();
		//aIStrategy = GameObject.FindGameObjectWithTag("AIStrategy").GetComponent<AIStrategy>();
		
	}
	
	void Start()
	{
		SwitchAnimation(AnimationMode.Run);
	}
	
	void FixedUpdate()
	{
		if(AILoopTimer<AILoop)
		{
			AILoopTimer=0.0f;
			Attack();
		}
		AILoopTimer+=Time.fixedDeltaTime;
	}
	
	void Attack()
	{
		if(targetingSystem.target)
		{
			navMeshAgent.enabled=true;
			navMeshAgent.SetDestination(targetingSystem.target.position);
			if(!ThrowAnimation.isPlaying)
			{
				
				SwitchAnimation(AnimationMode.Run);
				targetingSystem.UnFocusOnEnemy();
			}
			if(Vector3.Distance(targetingSystem.target.position,transform.position)<distanceLimit)
			{
				targetingSystem.FocusOnEnemy();
				if(fireRateTimer>1f)
				{	
					SwitchAnimation(AnimationMode.Throw);
					fireRateTimer=0.0f;
				}
				else
				{
					fireRateTimer+=Time.deltaTime*fireRate;
				}	
			}	
		}
		else
		{
			if(navMeshAgent.enabled)
			{
				navMeshAgent.enabled=false;
				SwitchAnimation(AnimationMode.Throw);	
			}
			else
			{
				KillAnimation(ThrowAnimation);
			}
			
			//aIStrategy.AssignTargets();
			
		}
	}
	
	
	void KillAnimation(Animation animation)
	{
		animation.Stop();
		animation.enabled=false;
	}
	
	void SwitchAnimation(AnimationMode animation)
	{
		if(animation == AnimationMode.Run)
		{
			RunAnimation.enabled=true;
			ThrowAnimation.enabled=false;
			
			RunRenderer.enabled=true;
			ThrowRenderer.enabled=false;
		}
		else if(animation == AnimationMode.Throw)
		{
			ThrowAnimation.enabled=true;
			RunAnimation.enabled=false;
			
			ThrowRenderer.enabled=true;
			RunRenderer.enabled=false;
			ThrowAnimation.Stop();
			ThrowAnimation.Rewind();
			ThrowAnimation.Play();
		}
	}
	

	
}
