using UnityEngine;
using System.Collections;

public class LoserGUI : MonoBehaviour {
	
	public GUIStyle style;
	public float ButtonOffsetX=10.0f;
	public float ButtonOffsetY=10.0f;
	
	public float ButtonOffset2X=10.0f;
	public float ButtonOffset2Y=10.0f;
	
	public float ButtonOffset3X=10.0f;
	public float ButtonOffset3Y=10.0f;
	
	//public GameObject saveSystem;
	
	void Awake()
	{
		//saveSystem = GameObject.FindGameObjectWithTag("SaveSystem");
	}
	
	void OnGUI()
	{
		if(GUI.Button(new Rect(Screen.width/ButtonOffsetX,Screen.height/ButtonOffsetY,Screen.width,Screen.height/10),"Continue",style))
		{
			Application.LoadLevel("Level");
		}
		else if(GUI.Button(new Rect(Screen.width/ButtonOffset2X,Screen.height/ButtonOffset2Y,Screen.width,Screen.height/10),"New Game",style))
		{
			//saveSystem.GetComponent<SaveSystem>().NewGame();
			PlayerPrefs.SetInt("Level",0);
			Application.LoadLevel("Level");
		}
		else if(GUI.Button(new Rect(Screen.width/ButtonOffset3X,Screen.height/ButtonOffset3Y,Screen.width,Screen.height/10),"Exit",style))
		{
			
			Application.Quit();
		}
	}
}
