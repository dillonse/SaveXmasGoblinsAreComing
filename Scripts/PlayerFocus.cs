using UnityEngine;
using System.Collections;

public class PlayerFocus : MonoBehaviour {
	
	private GameObject Mouse;
	void Start()
	{
		Mouse = new GameObject();
		Mouse.transform.position= Vector3.zero;
		
	}
	
	public void FocusOnTarget(Transform target)
	{
		transform.LookAt(target);
		//Debug.DrawRay(transform.position,transform.forward,Color.red);
	}
	
	public void FocusOnTarget()
	{
		// Find the difference vector pointing from the character to the cursor
		transform.Rotate(Input.GetAxis("Horizontal")*Vector3.up);

		/*Mouse.transform.position = Vector3.up * Input.mousePosition.y + Vector3.right * Input.mousePosition.x +Vector3.forward*0.0f;
		transform.LookAt(Mouse.transform);
		print(Input.mousePosition);
		print("----------------");*/
		
	}
}
