using UnityEngine;
using System.Collections;

public class WallAI : MonoBehaviour {

	public float Life=25.0f;
	public float Damage=1.0f;
	public Renderer Wall;
	public Renderer WallHole;
	public bool isDestroyed = false;
	public Collider WallHoleColl;
	
	public void Update()
	{
		if(Life<0&&Wall.enabled)
		{
			Life=0;
			Wall.enabled=false;
			WallHole.enabled=true;
			isDestroyed=true;
			collider.enabled=false;
			WallHoleColl.enabled=false;
		}
	}
	
	public void Hit()
	{
		Life-=Damage;
		if(Life<0&&Wall.enabled)
		{
			Life=0;
			Wall.enabled=false;
			WallHole.enabled=true;
			isDestroyed=true;
			collider.enabled=false;
		}
	}
}
