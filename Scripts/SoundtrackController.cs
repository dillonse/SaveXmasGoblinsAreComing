using UnityEngine;
using System.Collections;

public class SoundtrackController : MonoBehaviour {

	public AudioClip[] AudioArray;
	public SaveSystem saveSystem;
	public AudioSource audioSource;
	public AIStrategy aiStrategy;
	
	void Awake()
	{
		//saveSystem = GameObject.FindGameObjectWithTag("SaveSystem").GetComponent<SaveSystem>();
		audioSource = GetComponent<AudioSource>();
		aiStrategy = GameObject.FindGameObjectWithTag("AIStrategy").GetComponent<AIStrategy>();
	}
	
	void Start()
	{
		audioSource.clip = AudioArray[Random.Range(0,AudioArray.Length-1)];
		audioSource.Play();
		aiStrategy.LevelDuration = audioSource.clip.length;
//		print("length of audio is "+audioSource.clip.length);
	}
}
