using UnityEngine;
using System.Collections;

public class LoadingGUI : MonoBehaviour {

	public Texture[] textures;
	private SaveSystem savesystem;
	private GameObject SaveSystemGO;
	public GameObject plane;
	
	public GUIStyle style;
	public GUIStyle style2;
	public float ButtonOffsetX=10.0f;
	public float ButtonOffsetY=10.0f;
	
	public float ButtonOffset2X=10.0f;
	public float ButtonOffset2Y=10.0f;
	
	public float ButtonOffset3X=10.0f;
	public float ButtonOffset3Y=10.0f;
	
	
	void Start()
	{
		SaveSystemGO = GameObject.FindGameObjectWithTag("SaveSystem"); 
		
		if(SaveSystemGO)
			savesystem =GameObject.FindGameObjectWithTag("SaveSystem").GetComponent<SaveSystem>();
		
		if(savesystem)
			plane.renderer.material.mainTexture = textures[savesystem.level];
		else
			plane.renderer.material.mainTexture = textures[PlayerPrefs.GetInt("Level",0)];
		
	}
	
	void OnGUI()
	{
		if(GUI.Button(new Rect(Screen.width/ButtonOffsetX,Screen.height/ButtonOffsetY,Screen.width/5,Screen.height/10),"Continue",style))
		{
			Application.LoadLevel("Level");
		}
		else if(GUI.Button(new Rect(Screen.width/ButtonOffset3X,Screen.height/ButtonOffset3Y,Screen.width/10,Screen.height/10),"Exit",style))
		{
			Application.Quit();
		}
		
		GUI.TextArea(new Rect(Screen.width/ButtonOffset2X,Screen.height/ButtonOffset2Y,Screen.width,Screen.height/10),"(autosaved)",style2);
	}

}
