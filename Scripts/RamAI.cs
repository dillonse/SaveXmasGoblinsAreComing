using UnityEngine;
using System.Collections;

public class RamAI : MonoBehaviour {
	
	
	private TargetingSystem targetingSystem;
	private NavMeshAgent navMeshAgent;
	//public float AILoop = -3.0f;
	//private float AILoopTimer=0.0f;
	
	public enum AnimationMode {Dig,Run};
	
	public float DigRate=0.35f;
	private float DigRateTimer=0.0f;
	
	public GameObject RunRendererParent;
	public GameObject DigRendererParent;
	private Renderer[] RunRenderers;
	private Renderer[] DigRenderers;
	
	public Animation RunAnimation;
	public Animation DigAnimation;
	
	public Transform Wall;
	private float WallLeftLimit;
	private float WallRightLimit;
	private Vector3 WallTargetPos;
	private WallAI wallAI;
	
	public GameObject Goblin1;
	public GameObject Goblin2;
	
	public Transform Goblin1Transform;
	public Transform Goblin2Transform;
	private TargetingSystem Player;
	private bool hasDrivers=true;
	

	 public void Awake()
	{
		targetingSystem = GetComponent<TargetingSystem>();
		navMeshAgent = this.GetComponent<NavMeshAgent>();
		
		RunRenderers = RunRendererParent.GetComponentsInChildren<Renderer>();
		DigRenderers = DigRendererParent.GetComponentsInChildren<Renderer>();
		if(Wall)
		wallAI = Wall.GetComponent<WallAI>();
		Player = GameObject.FindGameObjectWithTag("Player").GetComponent<TargetingSystem>();
		
	}
	
	
	void Start()
	{
		Goblin2.GetComponent<TargetingSystem>().target = Player.targetee;
		SwitchAnimation(AnimationMode.Run);
		WallLeftLimit =Wall.position.z - (Wall.collider.bounds.size.z/2);
		WallRightLimit =Wall.position.z + (Wall.collider.bounds.size.z/2);
		WallTargetPos = transform.position.y*Vector3.up+Random.Range(WallLeftLimit,WallRightLimit)*Vector3.forward + Vector3.right*(Wall.position.x+Wall.collider.bounds.size.x/2);
	}
	
	void FixedUpdate()
	{
		if(hasDrivers)
			Dig();
		if((!Goblin1)&&(!Goblin2))
		{
			Stop();
			hasDrivers = false;
		}
	
	}
	
	
	void Dig()
	{
		if((!wallAI.isDestroyed)&&navMeshAgent.enabled)
		{
			navMeshAgent.SetDestination(WallTargetPos);
			navMeshAgent.stoppingDistance=3f;
			if(Vector2.Distance(new Vector2(transform.position.x,transform.position.z),new Vector2(WallTargetPos.x,WallTargetPos.z))<4.5f)
			{
				if(DigRateTimer>DigRate)
				{
					DigRateTimer=0.0f;
					wallAI.Life-=15f; 
				}
				DigRateTimer+=Time.deltaTime;
				SwitchAnimation(AnimationMode.Dig);
				transform.LookAt(Vector3.right*WallTargetPos.x+Vector3.forward*transform.position.z+Vector3.up*transform.position.y);
			}
			else
			{
				SwitchAnimation(AnimationMode.Run);
				//print("distance is "+Vector3.Distance(transform.position,WallTargetPos));
//				print("transform is "+transform.position + " and target is "+WallTargetPos);
			}	
		}
		else
		{
			Stop();
			if(Goblin1)
			{
				Goblin1.GetComponent<EnemyAI>().goblinMode = EnemyAI.GoblinMode.Attacker;
				Goblin1.GetComponent<NavMeshAgent>().enabled=true;
			}
			if(Goblin2)
			{
				Goblin2.GetComponent<EnemyAI>().goblinMode = EnemyAI.GoblinMode.Attacker;
				Goblin2.GetComponent<NavMeshAgent>().enabled=true;
			}
			hasDrivers=false;
		}
	}
	
	
	public void Stop()
	{
		RunAnimation.Stop();
		DigAnimation.Stop();
		RunAnimation.enabled=false;
		DigAnimation.enabled=false;
		navMeshAgent.enabled=false;
		
	}
	
	void SwitchAnimation(AnimationMode animation)
	{
		if(animation == AnimationMode.Run)
		{
			RunAnimation.enabled=true;
			DigAnimation.enabled=false;
			
			foreach(Renderer r in RunRenderers)
			{
				r.enabled = true;
			}
			
			foreach(Renderer r in DigRenderers)
			{
				r.enabled=false;
			}
		}

		else if(animation == AnimationMode.Dig)
		{
			DigAnimation.enabled=true;
			RunAnimation.enabled=false;
			
			foreach(Renderer r in RunRenderers)
			{
				r.enabled = false;
			}
			
			foreach(Renderer r in DigRenderers)
			{
				r.enabled = true;
			}
			

		}
	}
	

}
